#!/usr/local/bin/python3

import sys
import os
import requests
import re
import argparse
import subprocess
import time

def log(text):
	print(text, file=sys.stderr)

def shell(command, targetDir):
	log("Shell: " + command)
	p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, cwd=targetDir)
	(output, err) = p.communicate()
	p_status = p.wait()
	log("Command exit status/return code : " + str(p_status))
	decodedOutput = output.decode('utf-8')
	log("Output: " + decodedOutput)
	return decodedOutput

parser = argparse.ArgumentParser(description="Generate and await http certbot validation")
parser.add_argument("templatePath", help="The template file used to create the html pages to validate")
parser.add_argument("targetDir", help="The directory where the html pages are saved to")

args = parser.parse_args()

templatePath = args.templatePath
targetDir = args.targetDir

validation = os.environ["CERTBOT_VALIDATION"]
token = os.environ["CERTBOT_TOKEN"]
domain = os.environ["CERTBOT_DOMAIN"]
gitToken = os.environ["GITLAB_API_TOKEN"]

fileName = domain + ".html"

log("Validation " + validation)
log("Token " + token)
log("Domain " + domain)

def writeValidationFile():
	with open(templatePath, "r") as file :
		filedata = file.read()

	# Replace the target string
	filedata = filedata.replace("%TOKEN%", token)
	filedata = filedata.replace("%VALIDATION%", validation)

	# Write the file out again
	outPath = os.path.join(targetDir, fileName)
	with open(outPath, 'w') as file:
		file.write(filedata)
	log("Template file written")

# Push changes to origin
def gitPush():
	originUrl = shell("git remote get-url origin", targetDir)

	writeOriginUrl = re.sub("gitlab-ci-token:.*?@", "gitlab-ci-token:" + gitToken + "@", originUrl)
	print("Replaced for path: " + writeOriginUrl)

	shell("git remote add tokenOrigin " + writeOriginUrl, targetDir)
	#shell("git remote add sshOrigin git@gitlab.com:robinryf/robinryf.gitlab.io.git", targetDir)
	shell("git checkout master", targetDir)

	shell("git add " + fileName, targetDir)

	#shell("git config user.email gitlab-runner@baron.home", targetDir)
	#shell("git config user.name baron", targetDir)

	shell("git commit -m 'Add domain validation for " + domain + "'", targetDir)

	shell("git push tokenOrigin", targetDir)



# Wait for webpage to update

def waitForPageChange():
	doesSiteExist = False

	url = "http://" + domain + "/.well-known/acme-challenge/" + token
	log("url: " + url)
	while not doesSiteExist:
		r = requests.get(url)
		responseText = r.text.strip()
		#log("R: " + responseText + ", V: " + validation)
		if responseText == validation:
			doesSiteExist = True
		else:
			log("Page not found yet. Waiting...")
			time.sleep(30)

	log("Passed validation")

writeValidationFile()
gitPush()
waitForPageChange()
