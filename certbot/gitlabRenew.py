#!/usr/local/bin/python3

import os
import argparse
import subprocess


parser = argparse.ArgumentParser()

parser.add_argument("workingDir")
parser.add_argument("targetSiteDirectory", help="")
parser.add_argument("templateFile", help="")
parser.add_argument("contactEmail")
parser.add_argument("gitlabProjectId")
parser.add_argument("--domain", action="append")

args = parser.parse_args()

current_dir = os.getcwd()
working_dir = os.path.join(current_dir, args.workingDir)
script_dir = os.path.dirname(os.path.realpath(__file__))
auth_script_path = os.path.join(script_dir, "authenticator.py")

template_path = os.path.join(current_dir, args.templateFile)
target_site_dir = os.path.join(current_dir, args.targetSiteDirectory)

certbot_config_dir = os.path.join(working_dir, "config")
certbot_logs_dir = os.path.join(working_dir, "logs")
certbot_workspace_dir = os.path.join(working_dir, "workspace")

os.makedirs(certbot_config_dir, exist_ok=True)
os.makedirs(certbot_logs_dir, exist_ok=True)
os.makedirs(certbot_workspace_dir, exist_ok=True)
os.makedirs(target_site_dir, exist_ok=True)

certbot_parameters = [
    "certbot", "certonly",
    "--email", args.contactEmail,
    "--manual",
    "--manual-auth-hook", auth_script_path + " " + template_path + " " + target_site_dir,
    "--config-dir", certbot_config_dir,
    "--logs-dir", certbot_logs_dir,
    "--work-dir", certbot_workspace_dir,
    "--manual-public-ip-logging-ok",
    "--agree-tos"
]

for domain in args.domain:
    certbot_parameters.extend(["-d", domain])

print("parameters: " + str(certbot_parameters))
subprocess.run(certbot_parameters, stdout=subprocess.PIPE)

upload_script_path = os.path.join(script_dir, "uploadCert.py")

for domain in args.domain:
    upload_parameters = [
        "python3", upload_script_path,
        domain,
        args.gitlabProjectId,
        os.path.join(certbot_config_dir, "live/" + args.domain[0] + "/fullchain.pem"),
        os.path.join(certbot_config_dir, "live/" + args.domain[0] + "/privkey.pem"),
    ]

    print("upload paramters: " + str(upload_parameters))
    subprocess.run(upload_parameters, stdout=subprocess.PIPE)
