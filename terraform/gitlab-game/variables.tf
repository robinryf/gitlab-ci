variable "gitlab_project_id" {
  description = "Internal Id for the repo. No spaces or special symbols"
  type        = string
}

variable "gitlab_project_name" {
  description = "Display Name for GitLab project. Can contain spaces"
  type        = string
}