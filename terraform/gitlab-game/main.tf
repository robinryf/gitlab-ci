# Terraform configuration to set up providers by version.
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.9.1"
    }
  }
}

resource "gitlab_project" "main_project" {
  name = var.gitlab_project_id
  description = var.gitlab_project_name
  namespace_id = "4702535" # From robinbird-studios/projects GitLab group
  build_timeout = 10800 # 3 hours because of our super slow runner. Change once we have newer hardware
  default_branch = "master"
  shared_runners_enabled = false
  #group_runners_enabled = true
  initialize_with_readme = true
}