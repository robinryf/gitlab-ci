# Input variable definitions

variable "google_project_display_name" {
  description = "Display Name for Google Project. Can contain spaces"
  type        = string
}

variable "google_project_org_id" {
  description = "Internal Id for the Google Organisation on gcloud. All lower-case with hyphons"
  type        = string
}

variable "google_project_id" {
  description = "Internal Project Id for the Google/Firebase Project. All lower-case with hyphons"
  type        = string
}

variable "google_project_owner_email" {
  description = "Email of Owner account that should be added to fresh projects as owner"
  type        = string
}

variable "google_project_sdk_service_account_email" {
  description = "Email of Service account that that executes terraform/sdk/api calls"
  type        = string
}

variable "google_project_terraform_billing_project_id" {
  description = "The project used to carry out the Terraform actions. Somebody has to give permission to create a new project/resources on out cloud account"
  type        = string
}

variable "google_project_billing_account" {
  description = "Billing account id to use for the project created by this config"
  type        = string
  default     = "000000-000000-000000"
}

variable "app_display_name" {
  description = "Display Name for out internal tools to have a nice name"
  type        = string
}

variable "app_package_name" {
  description = "Package name Apple Style with reverse url scheme."
  type        = string
}

variable "apple_team_id" {
  description = "Team Id of the Apple Developer Program Account to use"
  type        = string
}