Build Server Manual configuration

Mac:
- Install Brew
- Clone the “gitlab-ci” repo
- Execute `configurations/setup-build-machine-mac.sh`

- Sign In to Mac App Store with apple@robinbird-studios.com
- Install Xcode

- brew services start gitlab-runner
- gitlab-runner register

- Create wakefile on baron /usr/gitlab-runner/home/wakefiles
