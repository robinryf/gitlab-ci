#!/bin/bash

echo "Prebuild script"
echo "Got Shared CI at ${SHARED_CI}"

#set -x
cd ${SHARED_CI}
#git pull

# Seperate to different file in case there are changes? So the updated version gets loaded and is not already in cache

# Activate python virtual environment to keep things seperate
python3 -m venv python/venv/

echo "Activating python virtual environment"
source python/venv/bin/activate

# Setup ssh key
if [[ ! -z "${GITLAB_RUNNER_SSH}" ]]; then chmod 600 ${GITLAB_RUNNER_SSH};SSH_ASKPASS=gitlab-ci/configurations/ssh_give_pass.sh ssh-add ${GITLAB_RUNNER_SSH} <<< "${GITLAB_RUNNER_SSH_PASSWORD}"; fi

# Unlock keychain
if [[ ! -z "${LOGIN_KEYCHAIN_PASS}" ]]; then security -v unlock-keychain -p ${LOGIN_KEYCHAIN_PASS} "login.keychain-db" ; else echo "No LOGIN_KEYCHAIN_PASS variable available. Keychain could be not AVAILABLE"; fi

FINISH_TIME=$(date +"%T")
echo "Finished prebuild script. Time: ${FINISH_TIME}"
# Since this script is sourced we have to restore any directory changes
cd ${CI_PROJECT_DIR}