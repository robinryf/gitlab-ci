#!/bin/sh

# Setup script for build machine. Is intended to be re-runnable withot breaking things

add_or_set_plist () {
    KEY=$1
    TYPE=$2
    VALUE=$3
    PLIST_PATH=$4
    echo "Setting plist ${KEY}:${VALUE} with type: ${TYPE} at path: ${PLIST_PATH}"
    PLIST_BUDDY=/usr/libexec/PlistBuddy
    ${PLIST_BUDDY} -x -c "Add :${KEY} ${TYPE} ${VALUE}" ${PLIST_PATH} || ${PLIST_BUDDY} -x -c "Set :${KEY} ${VALUE}" ${PLIST_PATH}
}

write_to_profile () {
    echo "Write $1=$2 to and .profile"
    echo "export $1=$2" >> ~/.profile
}


brew tap "homebrew/cask-versions"
#brew cask install adoptopenjdk8

#brew install --cask android-sdk
brew install --cask google-cloud-sdk
brew install --cask flutter
brew install --cask dotnet-sdk
brew tap isen-ng/dotnet-sdk-versions
brew install isen-ng/dotnet-sdk-versions/dotnet-sdk7
brew install --cask 1password/tap/1password-cli
brew tap airbytehq/tap
brew install abctl

# Setup Android SDK
#yes | sdkmanager --licenses
#sdkmanager "platform-tools" "platforms;android-28" "platforms;android-31" "platforms;android-31"

# Make sure our environment is loaded via gitlab, locally and via ssh
echo "source ~/.profile" > ~/.bash_profile
echo "source ~/.profile" > ~/.bashrc
echo "source ~/.profile" > ~/.zsh_profile
echo "source ~/.profile" > ~/.zshrc

#write_to_profile JAVA_HOME '/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home'
#write_to_profile JAVA_HOME '/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home'
write_to_profile JAVA_HOME '/opt/homebrew/opt/openjdk@17/libexec/openjdk.jdk/Contents/Home'
#write_to_profile ANDROID_NDK_HOME "/usr/local/share/android-sdk/ndk"
write_to_profile ANDROID_HOME '/usr/local/share/android-sdk'
write_to_profile UNITY_INSTALLATIONS '/Applications/Unity/Hub/Editor'
write_to_profile UNITY_EXECUTABLE_RELATIVE_PATH 'Unity.app/Contents/MacOS/Unity'
write_to_profile PATH '${HOME}/.gem/ruby/2.7.0/bin:${PATH}'
write_to_profile PATH '/usr/local/opt/ruby@2.7/bin:${PATH}'
write_to_profile LDFLAGS '-L/usr/local/opt/ruby@2.7/lib'
write_to_profile CPPFLAGS '-I/usr/local/opt/ruby@2.7/include'
write_to_profile PKG_CONFIG_PATH="/usr/local/opt/ruby@2.7/lib/pkgconfig"
write_to_profile LC_ALL en_US.UTF-8
write_to_profile LANG en_US.UTF-8

# Install all gems into user space
echo "gem: --user-install" > ~/.gemrc

# Reload environment
source ~/.profile

#npm login --registry https://npm.pkg.jetbrains.space/robinbird-studios/p/lib/robinbird-unity-registry/ --auth-type=legacy

# Set Unity Properties
UNITY_PREF_PATH=~/Library/Preferences/com.unity3d.UnityEditor5.x.plist
#add_or_set_plist AndroidNdkRootR16b string ${ANDROID_NDK_HOME} ${UNITY_PREF_PATH}
#add_or_set_plist AndroidNdkRootR19 string ${ANDROID_NDK_HOME} ${UNITY_PREF_PATH}
#add_or_set_plist AndroidNdkRootR21D string "${ANDROID_NDK_HOME}/21.3.6528147" ${UNITY_PREF_PATH}
#add_or_set_plist AndroidNdkRootR23B string "${ANDROID_NDK_HOME}/23.1.7779620" ${UNITY_PREF_PATH}
#add_or_set_plist AndroidSdkRoot string ${ANDROID_SDK_HOME} ${UNITY_PREF_PATH}
#add_or_set_plist JdkPath string ${JAVA_HOME} ${UNITY_PREF_PATH}
#add_or_set_plist Jdk11Path string "/usr/local/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home" ${UNITY_PREF_PATH}
#add_or_set_plist JdkUseEmbedded integer 0 ${UNITY_PREF_PATH}
#add_or_set_plist SdkUseEmbedded integer 0 ${UNITY_PREF_PATH}
#add_or_set_plist NdkUseEmbedded integer 0 ${UNITY_PREF_PATH}

# Install LaunchAgents
mkdir -p ~/Library/LaunchAgents
cp -f local.caffeinate.plist ~/Library/LaunchAgents/local.caffeinate.plist

# Setup gitlab runner
gitlab-runner install

# Setup XCode
sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer

# Disable Spotlight indexing on build machines
sudo mdutil -i off

# Install dependencies from Brewfile 
brew bundle --verbose
# Pin the gitlab runner so when we do auto updates via gitlab job the job does not fail
brew pin gitlab-runner

# Gem Setup
gem install bundler
bundle install --gemfile="../Gemfile" --path="~/.gem"

gem install cocoapods
gem install minitest
pod setup

# Npm Setup
npm install -g npm
npm install -g @angular/cli
npm install -g firebase-tools

# Unity Packagemanager setup
# TODO: Copy correct upm.toml file to build machine
# echo "________________________________________________________________"
# echo "YOU HAVE TO MANUALLY COPY THE UPM AUTH FILE TO THE BUILD MACHINE"
# echo "________________________________________________________________"
# echo "________________________________________________________________"

# echo "________________________________________________________________"
# echo "YOU HAVE TO CREATE A USERNAME AND WRITE TOKEN ON JETBRAINS SPACE"
# echo "________________________________________________________________"
# echo "________________________________________________________________"
# npm set @robinbird:registry https://npm.pkg.jetbrains.space/robinbird-studios/p/lib/robinbird-unity-registry
# npm addUser
# npm login --registry https://npm.pkg.jetbrains.space/robinbird-studios/p/lib/robinbird-unity-registry --auth-type legacy


# Python setup
python3 -m venv /Users/${USER}/gitlab-ci/python/venv/
source /Users/${USER}/gitlab-ci/python/venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r python/requirements.txt

# Docker setup
#sudo chown root:wheel $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
#sudo chmod u+s $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
#docker-machine create default --driver xhyve
docker network create gitlab-runner
cd buildserver
docker compose up -d
echo "127.0.0.1       op-connect-api" | sudo tee -a /etc/hosts

ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts

# Google Services Init
gcloud auth configure-docker
gcloud components install beta 
#firebase --open-sesame appdistribution

# Unity Hub Config (Disable Auto Update)
mkdir -p "$HOME/Library/Application Support/Unity/config"
echo "{\"hubDisableAutoUpdate\": true}" > "$HOME/Library/Application Support/Unity/config/services-config.json"

git lfs install --system

echo "If you want set the '[[runners]]/limit' variable in ~/.gitlab-runner/config.toml to '1' if this executer should only have one build running at a time"