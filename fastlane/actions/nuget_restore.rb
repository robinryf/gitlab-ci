module Fastlane
  module Actions
    module SharedValues
      NUGET_RESTORE_CUSTOM_VALUE = :NUGET_RESTORE_CUSTOM_VALUE
    end

    class NugetRestoreAction < Action
      def self.run(params)
        # fastlane will take care of reading in the parameter and fetching the environment variable:

        # sh "shellcommand ./path"

        # Actions.lane_context[SharedValues::NUGET_RESTORE_CUSTOM_VALUE] = "my_val"
        solution = params[:solution]

        command = "nuget restore \"#{solution}\""
        FastlaneCore::CommandExecutor.execute(command: command, print_all: true, print_command: true)

      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "A short description with <= 80 characters of what this action does"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
        # Define all options your action supports. 
        
        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :solution,
                                       env_name: "FL_NUGET_RESTORE_SOLUTION", # The name of the environment variable
                                       description: "Solution Path for NugetRestoreAction. Can point to a .sln or .csproj", # a short description of this parameter
                                       verify_block: proc do |value|
                                          UI.user_error!("No Solution for NugetRestoreAction given, pass using `solution: 'path'`") unless (value and not value.empty?)
                                          # UI.user_error!("Couldn't find file at path '#{value}'") unless File.exist?(value)
                                       end)
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['NUGET_RESTORE_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["@robinryf"]
      end

      def self.is_supported?(platform)
        # you can do things like
        # 
        #  true
        # 
        #  platform == :ios
        # 
        #  [:ios, :mac].include?(platform)
        # 

        true
      end
    end
  end
end
