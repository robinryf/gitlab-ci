# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :android do
  desc "Lane for Android Build Actions"

  lane :unity_package do
    gradle_properties = {}
    key_store = ENV["FL_GRADLE_KEY_STORE"]
    # Expects FL_GRADLE_PROJECT_DIR
    if (key_store)
      UI.message("Setting signing config for keystore #{key_store}")
      gradle_properties["android.injected.signing.store.file"] = key_store
      gradle_properties["android.injected.signing.store.password"] = ENV["FL_GRADLE_KEY_STORE_PASS"]
      gradle_properties["android.injected.signing.key.alias"] = ENV["FL_GRADLE_KEY_ALIAS"]
      gradle_properties["android.injected.signing.key.password"] = ENV["FL_GRADLE_KEY_PASS"]
    end

    gradle(
        properties: gradle_properties
      )

    apk_path = lane_context[SharedValues::GRADLE_APK_OUTPUT_PATH]
    aab_path = lane_context[SharedValues::GRADLE_AAB_OUTPUT_PATH]

    target_path = ENV["FL_PACKAGE_BUILD_PATH"]
    if apk_path
      UI.message("APK: #{apk_path} to #{target_path}")
      sh("mv #{apk_path} #{target_path}")
    end

    if aab_path
      UI.message("AAB: #{aab_path} to #{target_path}")
      sh("mv #{aab_path} #{target_path}")
    end
  end

  lane :xamarin_build do
    nuget_restore()

    msbuildr(
      targets: [:SignAndroidPackage]
    )
  end

  lane :upload_firebase_app_distribution do
    # Expects FIREBASEAPPDISTRO_APP
    # Expects FIREBASE_TOKEN
    firebase_app_distribution()
    write_app_distribution_env_file("android")
  end

  lane :upload_google_play do
    supply()
  end
end

platform :ios do
  desc "Lane for iOS Build Actions"

  lane :xamarin_create_icon do
    project_name = ENV["FL_PROJECT_NAME"]
    project_path = ENV["FL_PROJECT_PATH"]
    if project_name.nil? || project_path.nil?
      UI.user_error!("FL_PROJECT_NAME or FL_PROJECT_PATH is not specified")
    else
      appicon(appicon_image_file: "#{project_path}/AppMeta/AppIcon.png",
            appicon_devices: [:iphone, :ios_marketing, :ipad],
            appicon_path: "#{project_path}/#{project_name}/#{project_name}.iOS/Assets.xcassets")
    end
  end

  lane :xamarin_build do
    match(
      readonly: "true",
    )

    nuget_restore()

    msbuildr(
      targets: [:build],
      build_ipa: "true"
    )
  end

  lane :unity_package do
    match(
      readonly: "true",
    )

    export_method = ENV["GYM_EXPORT_METHOD"]
    export_method = export_method.sub("app-store", "appstore")

    # Expects FL_PROVISIONING_PROFILE
    update_code_signing_settings(
      use_automatic_signing: false,
      targets: "Unity-iPhone",
      # sigh_com.robinbirdstudios.secrets_development
      profile_uuid: ENV["sigh_" + ENV["FL_APP_IDENTIFIER"] + "_" + export_method],
      # sigh_com.robinbirdstudios.secrets_development_team-id
      team_id: ENV["sigh_" + ENV["FL_APP_IDENTIFIER"] + "_" + export_method + "_team-id"]
    )

    cocoapods(
      use_bundle_exec: false
    )
    gym()
  end

  lane :upload_firebase_app_distribution do
    # Expects FIREBASEAPPDISTRO_APP
    # Expects FIREBASE_TOKEN
    firebase_app_distribution()
    write_app_distribution_env_file("ios")
    upload_symbols_to_crashlytics()
  end

  lane :upload_app_store_connect do
    deliver(
      force: "true",
      api_key_path: ENV["APP_STORE_CONNECT_API_FASTLANE_KEY"],
      precheck_include_in_app_purchases: "false", # Disabled because using the AppStoreConnect API key does not yet support this
      )
  end

  lane :upload_app_store_metadata do
    deliver(
      metadata_path: ENV["DELIVER_METADATA_PATH"],
      screenshots_path: ENV["DELIVER_SCREENSHOT_PATH"],
      app_identifier: ENV["FL_APP_IDENTIFIER"],
      force: "true",
      api_key_path: ENV["APP_STORE_CONNECT_API_FASTLANE_KEY"],
      precheck_include_in_app_purchases: "false", # Disabled because using the AppStoreConnect API key does not yet support this
      )
  end

  lane :download_and_upload_debug_symbols do
    download_dsyms(
      username: CredentialsManager::AppfileConfig.try_fetch_value(:apple_id),
      app_identifier: ENV["FL_APP_IDENTIFIER"],
      build_number: ENV["CI_PIPELINE_IID"],
      output_directory: ENV["CI_PROJECT_DIR"],
      wait_for_dsym_processing: true,
      api_key_path: ENV["APP_STORE_CONNECT_API_FASTLANE_KEY"]
    )
    upload_symbols_to_crashlytics()
  end
end

platform :mac do

  lane :unity_package do
    match(
      readonly: "true",
    )

    gym(
      export_options: {
        provisioningProfiles: { 
          ENV["FL_APP_IDENTIFIER"] => ENV["PROVISIONING_PROFILE_NAME"]
        }
      }
    )
  end

  lane :steam_package do
    update_project_team
    unity_package
    notarize(
      verbose: true,
      api_key_path: ENV["APP_STORE_CONNECT_API_FASTLANE_KEY"]
    )
  end

end

def write_app_distribution_env_file(platform)
    firebase_distro = lane_context[SharedValues::FIREBASE_APP_DISTRO_RELEASE]
    UI.message("Set env from: #{firebase_distro}")
    File.open("#{ENV["CI_PROJECT_DIR"]}/appstore_#{platform}_upload.env", "w") do |file|
      file.write("FIREBASE_DISTRO_#{platform.upcase}_TESTER_LINK=#{firebase_distro[:testingUri]}")
      file.write("\n")
      file.write("FIREBASE_DISTRO_#{platform.upcase}_BUILD_VERSION=#{firebase_distro[:buildVersion]}")
      file.write("\n")
      file.write("FIREBASE_DISTRO_#{platform.upcase}_DISPLAY_VERSION=#{firebase_distro[:displayVersion]}")
    end
end