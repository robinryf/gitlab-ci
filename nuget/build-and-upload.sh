cd ${PROJECT_NAME}
nuget.sh restore ${PROJECT_NAME}.net35/${PROJECT_NAME}.net35.csproj -SolutionDirectory $(pwd)
msbuild /p:Configuration=Release ${PROJECT_NAME}.net35/${PROJECT_NAME}.net35.csproj
nuget.sh restore ${PROJECT_NAME}.net461/${PROJECT_NAME}.net461.csproj -SolutionDirectory $(pwd)
msbuild /p:Configuration=Release ${PROJECT_NAME}.net461/${PROJECT_NAME}.net461.csproj
dotnet restore ${PROJECT_NAME}.netstandard2.0/${PROJECT_NAME}.netstandard2.0.csproj
dotnet build -c "Release" ${PROJECT_NAME}.netstandard2.0/${PROJECT_NAME}.netstandard2.0.csproj

# Substitue bash variables in nuspec file
envsubst < ${PROJECT_NAME}.nuspec > ${PROJECT_NAME}.nuspec.tmp && mv ${PROJECT_NAME}.nuspec.tmp ${PROJECT_NAME}.nuspec
cat ${PROJECT_NAME}.nuspec
nuget-pack.sh ${PROJECT_NAME}.nuspec
nuget.sh sources add -name "VSTSNuget" -source ${NUGET_SOURCE} -username notneeded -password ${NUGET_PASSWORD}
nuget.sh push -Source "VSTSNuget" -NonInteractive -Timeout 600 -ApiKey VSTS ${PROJECT_NAME}.*.nupkg
