#!/usr/bin/env bash

${SHARED_CI}/unity3d/execute-unity.sh $(pwd)/UnityProject "-batchmode -createProject $(pwd)/UnityProject -logFile -quit"

# Checkout asset publisher
BASE_URL=`echo $CI_REPOSITORY_URL | sed "s;\/*$CI_PROJECT_PATH.*;;"`
REPO_URL="${BASE_URL}/robinryf/unity-asset-store-publisher.git"
git clone --recursive ${REPO_URL} $(pwd)/UnityProject/Assets/UnityAssetStorePublisher

Clone_ICON_PATH=$(pwd)/UnityProject/Assets/UnityAssetStorePublisher/AssetStoreTools/Editor/icon.png
ICON_DIR=$(pwd)/UnityProject/Assets/AssetStoreTools/Editor
ICON_PATH=${ICON_DIR}/icon.png
mkdir -p ${ICON_DIR}
# Moving icon to correct position because the Unity AssetStoreTools hard coded the path.. :facepalm:
mv ${Clone_ICON_PATH} ${ICON_PATH}

#find | sed 's|[^/]*/|- |g'

# Import package
${SHARED_CI}/unity3d/execute-unity.sh "$(pwd)/UnityProject" "-batchmode -projectPath $(pwd)/UnityProject -importPackage ${PACKAGE_PATH} -logFile ${CI_PROJECT_DIR}/logs/import-package.log -quit"

# Start package upload
UNITY_EXIT_CODE=0
${SHARED_CI}/unity3d/execute-unity.sh "$(pwd)/UnityProject" "-batchmode -nographics -projectPath $(pwd)/UnityProject -executeMethod AssetStorePublisher.Editor.AssetStoreBatchModeCommandline.BuildAndUploadAssetStorePackage -logFile ${CI_PROJECT_DIR}/logs/upload-package.log" || UNITY_EXIT_CODE=$?

if [ ${UNITY_EXIT_CODE} -eq 0 ]; then
  echo "Finished with success"
elif [ ${UNITY_EXIT_CODE} -eq 139 ]; then
  echo "Known exit crash happened. This is fine";
else
  echo "Unknown error code ${UNITY_EXIT_CODE}"
  exit 1
fi