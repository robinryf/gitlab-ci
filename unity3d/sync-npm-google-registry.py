import re
import requests
import subprocess
from pprint import pprint
from urllib.request import urlopen

regex = r"<td><a href=\"(.*?(com\.google\..*?)\/.*?-(\d+\.\d+\.\d+)\.tgz)\""
p = re.compile(regex)

url = 'https://developers.google.com/unity/archive'

html = urlopen(url).read()

decode = html.decode('utf-8')
matches = p.findall(decode)

# Keeps count of how many versions we found online.
versions = {}

versionNamesToUpload = []

for match in matches:
    print(match)
    packageUrl = match[0]
    packageName = match[1]
    packageVersion = match[2]
    versionName = packageName + "@" + packageVersion
    if packageName in versions:
        versions[packageName] = versions[packageName] + 1
    else:
        versions[packageName] = 1

    if versions[packageName] >= 3:
        print("Skip " + versionName + " because it is too old.")
        continue

    versionNamesToUpload.insert(0, (versionName, packageUrl))

print("Will attempt to upload versions:")
pprint(versionNamesToUpload)

for (versionName, packageUrl) in versionNamesToUpload:
    try:
        check = subprocess.check_output(["npm", "view", "--registry", "https://gitlab.com/api/v4/projects/5068071/packages/npm/", versionName])
        if len(check) != 0:
            print("Already have version " + versionName)
            continue
    except subprocess.CalledProcessError as e:
        print(e.output)

    print("Publishing " + versionName + " with url: " + packageUrl)
    if versionName == "com.google.firebase.app@8.8.0":
        print("Skipping versions because we screwed it up by manually uploading it.")
        continue
    res = subprocess.check_output(["npm", "publish", "--registry", "https://gitlab.com/api/v4/projects/5068071/packages/npm/",
                                packageUrl])
    for line in res.splitlines():
        print(line)

print("Finished")
