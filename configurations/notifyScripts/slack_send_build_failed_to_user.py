#!/usr/local/bin/python3
import os

import requests
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

gitlab_user_id = os.environ['GITLAB_USER_ID']
access_token = os.environ['GITLAB_API_TOKEN']

user_response = requests.get(f"https://gitlab.com/api/v4/users/{gitlab_user_id}?private_token={access_token}")
if user_response.status_code != 200:
	print(f"Could not get user {gitlab_user_id} code: {user_response.status_code}")
	exit(1)

slack_user_id = user_response.json()["skype"]

if slack_user_id == "":
	print("Not sending to slack because 'Skype' value is not setup for User on GitLab")
	exit(0)

client = WebClient(token=os.environ['SLACK_API_TOKEN'])
try:
	response = client.conversations_open(users=[slack_user_id])

	channel_id = response["channel"]["id"]
	project_name = os.environ['CI_PROJECT_NAME']
	pipeline_url = os.environ['CI_PIPELINE_URL']
	commit_ref_name = os.environ['CI_COMMIT_REF_NAME'] #branch/tag/sha
	commit_description = os.environ['CI_COMMIT_MESSAGE'].strip()
	client.chat_postMessage(channel=channel_id, text="Build Failed", blocks=
	[
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"*{project_name}* build failed :fire:"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"Ref: {commit_ref_name}\n Description: {commit_description}"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{pipeline_url}|Open Pipeline>"
			}
		}
	])
except SlackApiError as e:
	# You will get a SlackApiError if "ok" is False
	assert e.response["ok"] is False
	assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
	print(f"Got an error: {e.response['error']}")
	exit(1)
