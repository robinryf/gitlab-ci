#!/usr/local/bin/python3
import os

import requests
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

gitlab_user_id = os.environ['GITLAB_USER_ID']
access_token = os.environ['GITLAB_API_TOKEN']

user_response = requests.get(f"https://gitlab.com/api/v4/users/{gitlab_user_id}?private_token={access_token}")
if user_response.status_code != 200:
	print(f"Could not get user {gitlab_user_id} code: {user_response.status_code} error: {user_response.reason}")
	exit(1)

slack_user_id = user_response.json()["skype"]

if slack_user_id == "":
	print("Not sending to slack because 'Skype' value is not setup for User on GitLab")
	exit(0)

client = WebClient(token=os.environ['SLACK_API_TOKEN'])
try:
	response = client.conversations_open(users=[slack_user_id])

	channel_id = response["channel"]["id"]
	project_name = os.environ['CI_PROJECT_NAME']
	pipeline_iid = os.environ['CI_PIPELINE_IID']
	pipeline_url = os.environ['CI_PIPELINE_URL']
	commit_ref_name = os.environ['CI_COMMIT_REF_NAME'] #branch/tag/sha
	commit_description = os.environ['CI_COMMIT_MESSAGE'].strip()

	blocks = [
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"*{project_name.capitalize()}* build #{pipeline_iid} finished with success! :white_check_mark:"
			}
		},
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"Ref: {commit_ref_name}\nDescription: {commit_description}\n<{pipeline_url}|Open GitLab Pipeline>"
			}
		}
	]

	try:
		ios_url = os.environ['FIREBASE_DISTRO_IOS_TESTER_LINK']
	except KeyError:
		ios_url = None

	if ios_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{ios_url}|:green_apple: iOS {os.environ['FIREBASE_DISTRO_IOS_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_IOS_BUILD_VERSION']})>"
			}
		})

	try:
		android_url = os.environ['FIREBASE_DISTRO_ANDROID_TESTER_LINK']
	except KeyError:
		android_url = None

	if android_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{android_url}|:robot_face: Android  {os.environ['FIREBASE_DISTRO_ANDROID_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_ANDROID_BUILD_VERSION']})>"
			}
		})

	try:
		web_url = os.environ['FIREBASE_DISTRO_WEB_TESTER_LINK']
	except KeyError:
		web_url = None

	if web_url:
		blocks.append({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": f"<{web_url}|:globe_with_meridians: Web {os.environ['FIREBASE_DISTRO_WEB_DISPLAY_VERSION']} ({os.environ['FIREBASE_DISTRO_WEB_BUILD_VERSION']})>"
			}
		})

	#TODO: Add Admin tool?
	#TODO: Add Backend tool?

	client.chat_postMessage(
		channel=channel_id,
		text="Build Success",
		blocks=blocks
	)
	

except SlackApiError as e:
	# You will get a SlackApiError if "ok" is False
	assert e.response["ok"] is False
	assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
	print(f"Got an error: {e.response['error']}")
	exit(1)
