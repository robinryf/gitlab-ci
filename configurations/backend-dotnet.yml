
.backend:dotnet:debug:
    rules:
        - if: $CI_COMMIT_TAG == null && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "master" # merges/pushes to master should trigger this pipeline
        - if: $CI_COMMIT_TAG == null && $BACKEND != "" # manual trigger
    variables:
        DOCKER_IMAGE_NAME: gcr.io/${GCLOUD_PROJECT_NAME}/game-debug:${CI_COMMIT_SHORT_SHA}

.backend:dotnet:release:
    rules:
        - if: $CI_COMMIT_TAG != null
    variables:
        DOCKER_IMAGE_NAME: gcr.io/${GCLOUD_PROJECT_NAME}/game-release:${CI_COMMIT_SHORT_SHA}

### Test

.backend:dotnet:test:
    variables:
        PLATFORM: backend
        TEST_RESULT_DIR: ${CI_PROJECT_DIR}/testResults
    rules:
        - if: $BACKEND_TEST != "" || $CI_PIPELINE_SOURCE == "merge_request_event"


### Build

.backend:dotnet:build:
    stage: build
    extends:
        - .build
    script:
        # gcloud tool does not yet support Python 3.9
        - export PATH="/usr/local/opt/python@3.8/bin:$PATH"
        - alias python=/usr/local/opt/python@3.8/bin/python3
        - gcloud auth activate-service-account ${GOOGLE_CLOUD_SDK_EMAIL} --key-file=${GOOGLE_CLOUD_SDK_KEY_FILE}
        - export UNITY_LOG_PATH="${CI_PROJECT_DIR}/logs/unity_backend_export.log"
        - export UNITY_AUTOMATION_USER="op://RobinBirdDevOps/Unity Automation/username"
        - export UNITY_AUTOMATION_PASS="op://RobinBirdDevOps/Unity Automation/password"
        - op run -- sh -c '${SHARED_CI}/unity3d/execute-unity.sh ${CI_PROJECT_DIR}/Unity "-quit -batchmode -projectPath ${CI_PROJECT_DIR}/Unity -buildTarget Android -logfile ${UNITY_LOG_PATH} -cacheServerEnableDownload false -username ${UNITY_AUTOMATION_USER} -password ${UNITY_AUTOMATION_PASS} -executeMethod Talos.ECS.Editor.AssetPostprocessor.TalosConfigBackendAssetModificationProcessor.CreateConfigsForAll"'
        # Make sure to sync this with the test:backend:dotnet:compile test
        - DOCKER_BUILDKIT=1 docker build --platform linux/amd64 --no-cache-filter runtime -f Backend/${GAME_DIR_NAME}.Backend/Dockerfile ${CI_PROJECT_DIR} --build-arg="PUBLISH_CONFIGURATION=${BACKEND_PUBLISH_CONFIGURATION}" -t ${DOCKER_IMAGE_NAME}
        - mkdir -p builds/dotnet
        - docker save ${DOCKER_IMAGE_NAME} > builds/dotnet/build_backend_${CI_COMMIT_SHORT_SHA}.tar
    artifacts:
        paths:
            - builds/dotnet
            - ${CI_PROJECT_DIR}/logs
        expire_in: 1 days
    tags:
        - unity

backend:dotnet:build:debug:
    extends:
        - .build:debug
        - .backend:dotnet:debug
        - .backend:dotnet:build
    variables:
        BACKEND_PUBLISH_CONFIGURATION: Staging

backend:dotnet:build:release:
    extends:
        - .build:release
        - .backend:dotnet:release
        - .backend:dotnet:build
    variables:
        BACKEND_PUBLISH_CONFIGURATION: Release


### Upload

.backend:dotnet:upload:
    extends:
        - .upload
    stage: upload
    variables:
        GIT_SUBMODULE_STRATEGY: recursive # Need this here since we are executing dotnet code. TODO Maybe execute dotnet in the build step?
    script:
        - SERVICE_NAME=${BACKEND_VERSION_NAME}
        - echo "Using Service Name '${SERVICE_NAME}'"
        - echo "SERVICE_NAME=${SERVICE_NAME}" >> ${CI_PROJECT_DIR}/upload.env
        # Add the value to a dotenv file for environment step to pick it up. Does not work in clean up job
        - echo "BACKEND_DOMAIN=${BACKEND_DOMAIN}" >> ${CI_PROJECT_DIR}/upload.env
        - echo "BACKEND_URL=https://${BACKEND_DOMAIN}" >> ${CI_PROJECT_DIR}/upload.env
        # gcloud tool does not yet support Python 3.9
        - export PATH="/usr/local/opt/python@3.8/bin:$PATH"
        - alias python=/usr/local/opt/python@3.8/bin/python3
        - cd Backend/${GAME_DIR_NAME}.Backend
        - gcloud auth activate-service-account ${GOOGLE_CLOUD_SDK_EMAIL} --key-file=${GOOGLE_CLOUD_SDK_KEY_FILE}
        # TODO This is a problem because the docker image is deleted when retrying the job. Maybe use the exported Docker image from the artifacts.
        # TODO It is also better if the build and upload jobs would run on different machines
        - docker push ${DOCKER_IMAGE_NAME}
        - "gcloud run deploy ${SERVICE_NAME} 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--region europe-west1 
--service-account backend-runner@${GCLOUD_PROJECT_NAME}.iam.gserviceaccount.com
--max-instances=${BACKEND_MAX_INSTANCES} 
--revision-suffix ${CI_PIPELINE_IID} 
--image=${DOCKER_IMAGE_NAME} 
--update-env-vars RUNNING_IN_CLOUD_RUN=true,COMMIT_HASH=${CI_COMMIT_SHORT_SHA},SENTRY_RELEASE=\"${GCLOUD_PROJECT_NAME}@${SERVICE_NAME},ASPNETCORE_ENVIRONMENT=${BACKEND_ENVIRONMENT},SENTRY_ENVIRONMENT=${BACKEND_ENVIRONMENT}\""
        - gcloud run services update-traffic ${SERVICE_NAME} --to-latest --project ${GCLOUD_PROJECT_NAME} --region europe-west1
        - if [[ ${GCLOUD_DELETE_IMAGES} == "true" ]]; then gcloud container images delete ${DOCKER_IMAGE_NAME} --force-delete-tags --quiet; fi
        - CLOUD_RUN_URL=$(gcloud run services describe ${SERVICE_NAME} --project ${GCLOUD_PROJECT_NAME} --region europe-west1 --format yaml | shyaml get-value status.address.url)
        - CLOUD_RUN_HOSTNAME="${CLOUD_RUN_URL#https://}"
        - echo "Got cloud run url ${CLOUD_RUN_URL} and hostname ${CLOUD_RUN_HOSTNAME}"
        - echo "CLOUD_RUN_HOSTNAME=${CLOUD_RUN_HOSTNAME}" >> ${CI_PROJECT_DIR}/upload.env
        # Download unity match package dependency
        - UNITY_PACKAGE_MATH_VERSION=$(jq -r '.dependencies."com.unity.mathematics".version' ${CI_PROJECT_DIR}/Unity/Packages/packages-lock.json)
        - echo "Copy Math package with version ${UNITY_PACKAGE_MATH_VERSION}"
        - npm pack --registry https://packages.unity.com "com.unity.mathematics@${UNITY_PACKAGE_MATH_VERSION}"
        - tar -xvzf "com.unity.mathematics-${UNITY_PACKAGE_MATH_VERSION}.tgz"
        - mkdir -p ${CI_PROJECT_DIR}/Unity/Library/PackageCache
        - rm -rf "${CI_PROJECT_DIR}/Unity/Library/PackageCache/com.unity.mathematics"
        - mv package "${CI_PROJECT_DIR}/Unity/Library/PackageCache/com.unity.mathematics"
        - "dotnet run 
--project ${CI_PROJECT_DIR}/Backend/Talos.Backend/OpenApiGenerator/OpenApiGenerator.csproj 
--cloudRunHostUrl ${CLOUD_RUN_URL} 
--backendServiceName ${SERVICE_NAME} 
--googleCloudProjectId ${GCLOUD_PROJECT_NAME} 
--googleCloudProjectNumber ${GCLOUD_PROJECT_NUMBER}
--outputFile \"${CI_PROJECT_DIR}/openapi-run.yaml\""
        - gcloud endpoints services deploy "${CI_PROJECT_DIR}/openapi-run.yaml" --project ${GCLOUD_PROJECT_NAME}
        - gcloud services enable ${CLOUD_RUN_HOSTNAME} --project ${GCLOUD_PROJECT_NAME}
        - CONFIG_ID=$(gcloud endpoints configs list --service=${CLOUD_RUN_HOSTNAME} --limit 1 --format=yaml | shyaml get-value id)
        - echo "Extracted ConfigId ${CONFIG_ID}"
        - BUILD_IMAGE_OUTPUT=$(${SHARED_CI}/gcloud/gcloud_build_image.sh -s ${CLOUD_RUN_HOSTNAME} -c ${CONFIG_ID} -p ${GCLOUD_PROJECT_NAME})
        - echo ${BUILD_IMAGE_OUTPUT} > ${CI_PROJECT_DIR}/build_endpoints_image.log
        #- echo ${BUILD_IMAGE_OUTPUT}
        #- >
        #    regex=".tgz (gcr.io/.*) SUCCESS"
        #- echo ${regex}
        #- IMAGE_NAME=$([[ $BUILD_IMAGE_OUTPUT =~ $regex ]] && echo "${BASH_REMATCH[1]}" || echo "none-none")
        - ESP_VERSION="no-new-use-public-image-2.48"
        - IMAGE_NAME="gcr.io/${GCLOUD_PROJECT_NAME}/endpoints-runtime-serverless:${ESP_VERSION}-${CLOUD_RUN_HOSTNAME}-${CONFIG_ID}"
        - ENDPOINTS_SERVICE_NAME="${SERVICE_NAME}-endpoints"
        - echo "ENDPOINTS_SERVICE_NAME=${ENDPOINTS_SERVICE_NAME}" >> ${CI_PROJECT_DIR}/upload.env
        - "gcloud run deploy ${ENDPOINTS_SERVICE_NAME} 
--image=${IMAGE_NAME} 
--allow-unauthenticated 
--platform managed 
--region europe-west1 
--project=${GCLOUD_PROJECT_NAME}"
        - gcloud run services update-traffic ${ENDPOINTS_SERVICE_NAME} --to-latest --project ${GCLOUD_PROJECT_NAME} --region europe-west1
        - if [[ ${GCLOUD_DELETE_IMAGES} == "true" ]]; then gcloud container images delete ${IMAGE_NAME} --force-delete-tags --quiet; fi
        # Disabled for now. Docs say
        # This step is not needed if ESPv2 Cloud Run service is deployed without using the flag --service-account and the backend service BACKEND_SERVICE_NAME is deployed in the ESP_PROJECT_NUMBER project.
        # We are not using this flag so things should be fine
        #- GCLOUD_DEFAULT_COMPUTE_SERVICE_ACCOUNT=UNKNOWN
        #- "gcloud run services add-iam-policy-binding ${SERVICE_NAME} 
#--member \"serviceAccount:${GCLOUD_DEFAULT_COMPUTE_SERVICE_ACCOUNT}\" 
#--role \"roles/run.invoker\" 
#--platform managed 
#--region europe-west1 
#--project ${GCLOUD_PROJECT_NAME}"
        - "gcloud beta run domain-mappings create 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--region=europe-west1 
--service ${ENDPOINTS_SERVICE_NAME} 
--domain ${BACKEND_DOMAIN} 
|| echo \"Mapping already created.\""
        - docker image rm -f ${DOCKER_IMAGE_NAME}
        - LATEST_DOMAIN=latest.${GCLOUD_PROJECT_NAME}.${DOMAIN}
        - "if [[ ${CI_COMMIT_TAG} != \"\" ]]; then echo \"Detected release. Updating latest domain mapping\" ;gcloud beta run domain-mappings delete 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--region=europe-west1 
--quiet 
--domain ${LATEST_DOMAIN}; 
gcloud beta run domain-mappings create 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--region=europe-west1 
--service ${ENDPOINTS_SERVICE_NAME} 
--domain ${LATEST_DOMAIN}; fi"
    artifacts:
        paths:
            - ${CI_PROJECT_DIR}/build_endpoints_image.log
        when: always



.backendcleanup:
    variables:
        GIT_STRATEGY: none
    script:
        - echo "Stopping environment at url ${BACKEND_DOMAIN}"
        #- cat "${CI_PROJECT_DIR}/upload.env"
        #- source ${CI_PROJECT_DIR}/upload.env
        #- SERVICE_NAME=${BACKEND_VERSION_NAME}
        #- ENDPOINTS_SERVICE_NAME="${SERVICE_NAME}-endpoints"
        - "gcloud beta run domain-mappings delete 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--region=europe-west1 
--quiet 
--domain ${BACKEND_DOMAIN}"
        - "gcloud run services delete ${SERVICE_NAME} 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--quiet 
--region europe-west1"
        - echo "Deleting service ${ENDPOINTS_SERVICE_NAME}"
        - "gcloud run services delete ${ENDPOINTS_SERVICE_NAME} 
--project ${GCLOUD_PROJECT_NAME} 
--platform=managed 
--quiet 
--region europe-west1"
        - "gcloud endpoints services delete ${CLOUD_RUN_HOSTNAME} 
--project ${GCLOUD_PROJECT_NAME} 
--quiet"

backend:dotnet:upload:debug:
    extends:
        - .upload:debug
        - .backend:dotnet:debug
        - .backend:dotnet:upload
    variables:
        BACKEND_ENVIRONMENT: Development
        BACKEND_MAX_INSTANCES: 1
        GCLOUD_DELETE_IMAGES: "true" # Can be set to false so we can still deploy a previous revision on Cloud Run
    needs: ["backend:dotnet:build:debug"]
    artifacts:
        reports:
            dotenv: upload.env
        paths:
            - ${CI_PROJECT_DIR}/openapi-run.yaml
            - ${CI_PROJECT_DIR}/upload.env
        when: always
    environment:
        name: ${CI_COMMIT_REF_SLUG}/backend
        url: $BACKEND_URL
        on_stop: backend:dotnet:upload:cleanup:debug

backend:dotnet:upload:cleanup:debug:
    extends:
        - .upload:debug
        - .backend:dotnet:debug
        - .backend:dotnet:upload
    variables:
        !reference [.backendcleanup, variables]
    script:
        - !reference [.backendcleanup, script]
    dependencies:
        - backend:dotnet:upload:debug
    stage: manual
    environment:
        name: ${CI_COMMIT_REF_SLUG}/backend
        action: stop
    rules:
        - if: $CI_COMMIT_TAG == null && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "master" # merges/pushes to master should trigger this pipeline
          when: manual
        - if: $CI_COMMIT_TAG == null && $BACKEND != "" # manual trigger
          when: manual
    allow_failure: true


backend:dotnet:upload:release:
    extends:
        - .upload:release
        - .backend:dotnet:release
        - .backend:dotnet:upload
    variables:
        BACKEND_ENVIRONMENT: Production
        BACKEND_MAX_INSTANCES: 50
        GCLOUD_DELETE_IMAGES: "false" # Can be set to false so we can still deploy a previous revision on Cloud Run
    needs: ["backend:dotnet:build:release"]
    artifacts:
        reports:
            dotenv: upload.env
        paths:
            - ${CI_PROJECT_DIR}/openapi-run.yaml
            - ${CI_PROJECT_DIR}/upload.env
        when: always
    environment:
        name: ${CI_COMMIT_REF_SLUG}/backend
        url: $BACKEND_URL
        on_stop: backend:dotnet:upload:cleanup:release

backend:dotnet:upload:cleanup:release:
    extends:
        - .upload:release
        - .backend:dotnet:release
        - .backend:dotnet:upload
    variables:
        !reference [.backendcleanup, variables]
    script:
        - !reference [.backendcleanup, script]
    dependencies:
        - backend:dotnet:upload:release
    stage: manual
    environment:
        name: ${CI_COMMIT_REF_SLUG}/backend
        action: stop
    rules:
        - if: $CI_COMMIT_TAG != null
          when: manual
    allow_failure: true
