#!/usr/local/bin/python3

import sys
import os
import re
import subprocess

def log(text):
	print(text, file=sys.stderr)

def shell(command, targetDir):
	log("Shell: " + command)
	p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, cwd=targetDir)
	(output, err) = p.communicate()
	p_status = p.wait()
	log("Command exit status/return code : " + str(p_status))
	decodedOutput = output.decode('utf-8')
	log("Output: " + decodedOutput)
	return decodedOutput

# Push changes to origin
def gitPush(targetDir, message, branch):
	originUrl = shell("git remote get-url origin", targetDir)

	gitToken = os.getenv('GITLAB_API_TOKEN')
	writeOriginUrl = re.sub("gitlab-ci-token:.*?@", "gitlab-ci-token:" + gitToken + "@", originUrl)
	print("Replaced for path: " + writeOriginUrl)

	shell("git remote add tokenOrigin " + writeOriginUrl, targetDir)
	#shell("git remote add sshOrigin git@gitlab.com:robinryf/robinryf.gitlab.io.git", targetDir)

	shell("git stash", targetDir)

	shell(f"git checkout {branch}", targetDir)
	shell("git pull", targetDir)
	shell("git stash pop", targetDir)
	shell("git add --all", targetDir)

	#shell("git config user.email gitlab-runner@baron.home", targetDir)
	#shell("git config user.name baron", targetDir)

	shell(f"git commit -m '{message}'", targetDir)

	shell("git push tokenOrigin", targetDir)
