certifi==2020.12.5
chardet==4.0.0
idna==2.10
requests
slack_sdk
argparse
six
urllib3
